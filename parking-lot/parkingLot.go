package main

import (
	"fmt"
)

type parkingLotSystem interface {
	showAvailableSlots() []slot
	releaseSlot(id int) bool
	showAllSlots() []*slot
	addFloor()
	addSlot(sl slot, fl int)
	getTickets() []ticket
	addTickets(tick ticket)
}

type vehicle interface {
	bookSlot(p parkingLotSystem) ticket
}

type car struct {
	color     string
	ownerName string
	plateNum  string
}

type bike struct {
	color     string
	ownerName string
	plateNum  string
}

func (c *car) bookSlot(p parkingLotSystem) ticket {
	slots := p.showAllSlots()
	var tick *ticket
	for i := range slots {
		if !slots[i].booked && slots[i].slotType == "car" {
			tick = new(ticket)
			tick.id = len(p.getTickets())
			tick.slotId = slots[i].id
			p.addTickets(*tick)
		}
	}
	return *tick
}

func (p *parkingLot) addTickets(tick ticket){
	p.tickets = append(p.tickets, tick)
}
func (p *parkingLot) getTickets() []ticket{
	return p.tickets
}

func (c *bike) bookSlot(p parkingLotSystem) ticket {
	slots := p.showAllSlots()
	var tick *ticket
	for i := range slots {
		if !slots[i].booked && slots[i].slotType == "bike" {
			tick = new(ticket)
			tick.id = len(p.getTickets())
			tick.name = c.ownerName
			tick.slotId = slots[i].id
			slots[i].booked = true
			p.addTickets(*tick)
			break
		}
	}
	return *tick
}

type parkingLot struct {
	park    []floor
	tickets []ticket
}

type floor struct {
	slots []slot
}

func (p *parkingLot) addFloor() {
	f := new(floor)
	p.park = append(p.park, *f)
}

func (p *parkingLot) addSlot(sl slot, fl int) {
	p.park[fl].slots = append(p.park[fl].slots, sl)
}

func (p *parkingLot) showAvailableSlots() []slot {
	fmt.Println("the available slots are: ")
	sl := []slot{}
	for i := 0; i < len(p.park); i++ {
		for j := 0; j < len(p.park[i].slots); j++ {
			if !p.park[i].slots[j].status.booked {
				sl = append(sl, p.park[i].slots[j])
			}
		}
	}
	return sl
}

func (p *parkingLot) showAllSlots() []*slot {
	fmt.Println("the available slots are: ")
	sl := []*slot{}
	for i := 0; i < len(p.park); i++ {
		for j := 0; j < len(p.park[i].slots); j++ {
			sl = append(sl, &p.park[i].slots[j])
		}
	}
	return sl
}

func (p *parkingLot) releaseSlot(id int) bool {
	slots := p.showAllSlots()
	for _, sl := range slots {
		if sl.id == id {
			sl.booked = false
			return true
		}
	}
	return true
}

type slot struct {
	slotType string
	id       int
	status
}

type status struct {
	booked bool
}

type ticket struct {
	id     int
	name   string
	slotId int
}

func NewParkingLot() parkingLotSystem {
	slotIdCounter := 0
	parkingLot := new(parkingLot)
	parkingLot.addFloor()
	sl := new(slot)
	sl.id = slotIdCounter + 1
	sl.slotType = "car"
	parkingLot.addSlot(*sl, 0)
	parkingLot.addFloor()
	sl = new(slot)
	sl.id = slotIdCounter + 1
	slotIdCounter++
	sl.slotType = "bike"
	parkingLot.addSlot(*sl, 1)
	sl = new(slot)
	sl.id = slotIdCounter + 1
	slotIdCounter++
	sl.slotType = "bike"
	parkingLot.addSlot(*sl, 1)

	return parkingLot
}

func registerNewVehicle(tp string, ownerName string, color string, plateNum string) vehicle {
	if tp == "car" {
		return &car{color: color, ownerName: ownerName, plateNum: plateNum}
	}
	if tp == "bike" {
		return &bike{color: color, ownerName: ownerName, plateNum: plateNum}
	}
	return nil
}

func main() {
	v := registerNewVehicle("bike", "guru", "color", "0980-AB")
	p := NewParkingLot()
	ticket := v.bookSlot(p)
	fmt.Println(ticket)
	ticket = v.bookSlot(p)
	fmt.Printf("%+v", ticket)
}
